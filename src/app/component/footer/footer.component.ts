import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  aniActual = new Date;
  diasemana = this.aniActual.getDay();
  dia = this.aniActual.getDate();
  mes = this.aniActual.getMonth();
  anio = this.aniActual.getFullYear();



  dias = new Array ("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado")
  meses = new Array ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octure", " Noviembre", "Diciembre");

  fecha = this.dias[this.diasemana]+" "+this.dia + "de "+this.meses[this.mes]+" de "+ this.anio;



  constructor() { }

  ngOnInit(): void {
  }

}
